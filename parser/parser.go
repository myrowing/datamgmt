package parser

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/pkg/errors"
	l "github.com/sirupsen/logrus"
	"gitlab.com/myrowing/datamgmt/model"
	"gitlab.com/myrowing/datamgmt/parser/nkspdcoach"
)

var log *l.Entry = l.WithFields(l.Fields{"srv": "parser"})

// Parser represents a parser for files that contain training data
type Parser interface {
	Read(*model.Team, string) (*model.Training, error)
}

// New creates a new parser based on the content of the training file. If the
// file format is not known (i.e. there's no parser), an error is returned
func New(path string) (p Parser, err error) {
	log.Tracef("creating a parser for '%s' ...", path)

	file, err := os.Open(path)
	if err != nil {
		err = errors.Wrapf(err, "cannot open training file '%s'", path)
		log.Error(err)
		return
	}
	defer file.Close()

	// check if path is a data file of an NK SpeedCoach (4-th line must contain
	// the correct model information
	sc := bufio.NewScanner(file)
	sc.Split(bufio.ScanLines)
	for i := 0; i < 4; i++ {
		sc.Scan()
	}
	if strings.Contains(sc.Text(), "Model:,SpeedCoach") {
		log.Tracef("created an NK SpeedCoach parser for '%s'", path)
		return new(nkspdcoach.Parser), nil
	}

	err = fmt.Errorf("'%s' is not a known data file format", path)
	log.Error(err)
	return
}
