package nkspdcoach

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	l "github.com/sirupsen/logrus"
	"gitlab.com/myrowing/datamgmt/model"
)

var log *l.Entry = l.WithFields(l.Fields{"srv": "NKSpeedCoachparser"})

// input represents the source for speed information
type input string

const inputGPS input = "gps" // speed data source: GPS

// Parser is the parser for data files from NK SpeedCoach
type Parser struct {
	in                input
	distUnit          model.Unit
	distPerStrokeUnit model.Unit
	speedUnit         model.Unit
	splitUnit         model.Unit
}

// Read retrieves all data of a training from the data file behind path
func (p Parser) Read(tm *model.Team, path string) (tr *model.Training, err error) {
	file, err := os.Open(path)
	if err != nil {
		err = errors.Wrapf(err, "cannot open training file '%s'", path)
		log.Error(err)
		return
	}
	defer file.Close()

	tr = new(model.Training)
	tr.Tm = tm
	tr.Path = path

	sc := bufio.NewScanner(file)
	sc.Split(bufio.ScanLines)

	var td *model.TrainingDetail

	for counter := 1; sc.Scan(); counter++ {
		line := sc.Text()

		switch counter {
		case 3:
			// parse boat name
			tr.Boat = extractString(line, `Boat ID:,(.+)`)
		case 4:
			// parse start time
			if tr.Start, err = parseTime("start time", extractString(line, `Start Time:,([^,]+),`), tr.Tm.TimeZone, tm.SrcTimeFormat); err != nil {
				return
			}
		case 6:
			// parse system of units
			srcUnits := extractString(line, `System of Units:,([^/]+)/`)
			switch srcUnits {
			case "Meters":
				p.distUnit = model.Meters
				p.distPerStrokeUnit = model.Meters
				p.speedUnit = model.MPerS
				p.splitUnit = model.Split500m
			case "Kilometers":
				p.distUnit = model.KM
				p.distPerStrokeUnit = model.Meters
				p.speedUnit = model.KMPerH
				p.splitUnit = model.Split500m
			case "Miles":
				p.distUnit = model.Miles
				p.distPerStrokeUnit = model.Feet
				p.speedUnit = model.MiPerH
				p.splitUnit = model.Split1mi
			default:
				err = fmt.Errorf("'%s' is not a valid system of units", srcUnits)
				log.Error(err)
				return
			}
		case 7:
			// parse speed input
			p.in = input(strings.ToLower(extractString(line, `Speed Input:,([^,]+),`)))
		case 16:
			// parse summary and totals
			if err = p.readTotals(tr, line); err != nil {
				return
			}
		default:
			if counter > 30 {
				if td, err = p.addDetail(tr, line, td); err != nil {
					return
				}
			}
		}
	}

	return
}

// addDetail creates new training details
func (p Parser) addDetail(tr *model.Training, line string, prev *model.TrainingDetail) (td *model.TrainingDetail, err error) {
	values := strings.Split(line[0:len(line)-1], ",")
	td = new(model.TrainingDetail)

	// parse cumulated distance
	s := ""
	if p.in == inputGPS {
		s = values[1]
	} else {
		s = values[2]
	}
	if td.CumDist, err = parseFloat("distance", s); err != nil {
		return
	}
	td.CumDist = model.Convert(td.CumDist, p.distUnit, tr.Tm.TrgUnitDist)

	// parse time`
	var elapsed time.Duration
	if elapsed, err = parseDuration("elapsed time", values[3]); err != nil {
		return
	}
	td.T = tr.Start.Add(elapsed)

	// calculate duration
	var start time.Time
	if prev == nil {
		start = tr.Start
	} else {
		start = prev.T
	}
	td.D = time.Duration(td.T.UnixNano() - start.UnixNano())

	// parse split
	if p.in == inputGPS {
		s = values[4]
	} else {
		s = values[6]
	}
	if td.Split, err = parseDuration("split", s); err != nil {
		return
	}
	td.Split = time.Duration(model.Convert(float64(td.Split), p.splitUnit, tr.Tm.TrgUnitSplit))

	// parse speed
	if p.in == inputGPS {
		s = values[5]
	} else {
		s = values[7]
	}
	if td.Speed, err = parseFloat("speed", s); err != nil {
		return
	}
	td.Speed = model.Convert(td.Speed, p.speedUnit, tr.Tm.TrgUnitSpeed)

	// parse stroke rate
	if td.StrokeRate, err = parseFloat("stroke rate", values[8]); err != nil {
		return
	}

	// parse distance per stroke
	if p.in == inputGPS {
		s = values[10]
	} else {
		s = values[11]
	}
	if td.DistPerStroke, err = parseFloat("distance per stroke", s); err != nil {
		return
	}
	td.DistPerStroke = model.Convert(td.DistPerStroke, p.distPerStrokeUnit, tr.Tm.TrgUnitDistPerStroke)

	// parse latitude
	if td.Latitute, err = parseFloat("latitude", values[22]); err != nil {
		return
	}

	// parse longitude
	if td.Longitude, err = parseFloat("longitude", values[23]); err != nil {
		return
	}

	tr.Details = append(tr.Details, *td)
	return
}

// readTotals retrieves total and average numbers for the training from the
// header section of the data file.
func (p Parser) readTotals(tr *model.Training, line string) (err error) {
	values := strings.Split(line[0:len(line)-1], ",")

	// parse total distance
	s := ""
	if p.in == inputGPS {
		s = values[1]
	} else {
		s = values[2]
	}
	if tr.TotalDist, err = parseFloat("total distance", s); err != nil {
		return
	}
	tr.TotalDist = model.Convert(tr.TotalDist, p.distUnit, tr.Tm.TrgUnitDist)

	// parse elapsed time and set end time
	var elapsed time.Duration
	if elapsed, err = parseDuration("elapsed time", values[3]); err != nil {
		return
	}
	tr.End = tr.Start.Add(elapsed)

	// parse split
	if p.in == inputGPS {
		s = values[4]
	} else {
		s = values[6]
	}
	if tr.AvgSplit, err = parseDuration("split", s); err != nil {
		return
	}
	tr.AvgSplit = time.Duration(model.Convert(float64(tr.AvgSplit), p.splitUnit, tr.Tm.TrgUnitSplit))

	// parse speed
	if p.in == inputGPS {
		s = values[5]
	} else {
		s = values[7]
	}
	if tr.AvgSpeed, err = parseFloat("speed", s); err != nil {
		return
	}
	tr.AvgSpeed = model.Convert(tr.AvgSpeed, p.speedUnit, tr.Tm.TrgUnitSpeed)

	// parse distance per stroke
	if p.in == inputGPS {
		s = values[10]
	} else {
		s = values[11]
	}
	if tr.AvgDistPerStroke, err = parseFloat("distance per stroke", s); err != nil {
		return
	}
	tr.AvgDistPerStroke = model.Convert(tr.AvgDistPerStroke, p.distPerStrokeUnit, tr.Tm.TrgUnitDistPerStroke)

	// parse stroke rate
	if tr.AvgStrokeRate, err = parseFloat("stroke rate", values[8]); err != nil {
		return
	}

	// parse total number of strokes
	if tr.TotalStrokes, err = parseInt("total strokes", values[9]); err != nil {
		return
	}

	return
}

// extractString extracts a sub string from s depending on the regular
// expression r
func extractString(s string, r string) (res string) {
	re := regexp.MustCompile(r)
	aa := re.FindAllStringSubmatch(s, -1)
	for _, a := range aa {
		if len(a) > 0 && len(a[1]) > 0 && a[1] != "---" {
			return a[1]
		}
	}
	return
}

// parseDuration extracts duration data from value of the field named name
func parseDuration(name, value string) (d time.Duration, err error) {
	if name == "---" {
		return 0.0, nil
	}

	if d, err = time.ParseDuration(value[:2] + "h" + value[3:5] + "m" + value[6:] + "s"); err != nil {
		err = errors.Wrapf(err, "cannot parse %s '%s'", name, value)
		log.Error(err)
	}
	return
}

// parseFloat extracts a float number from value of the field named name
func parseFloat(name, value string) (f float64, err error) {
	if name == "---" {
		return 0.0, nil
	}

	if f, err = strconv.ParseFloat(value, 64); err != nil {
		err = errors.Wrapf(err, "cannot parse %s '%s'", name, value)
		log.Error(err)
	}
	return
}

// parseInt extracts an integer number from value of the field named name
func parseInt(name, value string) (i int, err error) {
	if name == "---" {
		return 0, nil
	}

	if i, err = strconv.Atoi(value); err != nil {
		err = errors.Wrapf(err, "cannot parse %s '%s'", name, value)
		log.Error(err)
	}
	return
}

// parseTime extracts a time value from value of the field named name. value
// is in time zone tz. The result is in UTC
func parseTime(name, value, tz, format string) (t time.Time, err error) {
	if len(value) == 0 {
		return
	}

	// determine date layout:
	// The layout is either MM/DD/YY or DD/MM/YY. At first, it is attempted to
	// derive the layout from the content of the date field. If that's not
	// possible, the date format from the team config is taken.
	var layout string
	if value[3:5] > "12" {
		layout = "01/02/06"
	} else if value[:2] > "12" {
		layout = "02/01/06"
	} else {
		layout = strings.Replace(format, "DD", "02", 1)
		layout = strings.Replace(layout, "MM", "01", 1)
		layout = strings.Replace(layout, "YY", "06", 1)
	}
	layout += " 15:04"

	// parse time and convert it to UTC
	var loc *time.Location
	if loc, err = time.LoadLocation(tz); err != nil {
		err = errors.Wrapf(err, "cannot parse time '%s'", value)
		log.Error(err)
		return
	}
	t, err = time.ParseInLocation(layout, value, loc)
	if err != nil {
		err = errors.Wrapf(err, "cannot parse time '%s'", value)
		log.Error(err)
		return
	}
	t = t.UTC()

	return
}
