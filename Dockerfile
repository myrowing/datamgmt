# syntax=docker/dockerfile:1

## build step
FROM golang:1.17-alpine
COPY . $GOPATH/src/gitlab.com/myrowing/datamgmt/
WORKDIR $GOPATH/src/gitlab.com/myrowing/datamgmt/
RUN apk add --no-cache make bash
RUN make; make install


## deploy step
FROM alpine:latest
WORKDIR /
COPY --from=0 /usr/bin/datamgmt /datamgmt
# datamgmt requires time zones
RUN apk add --no-cache tzdata 
ENTRYPOINT ["/datamgmt"]
