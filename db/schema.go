package db

// DB statements for the creation of all myrowing objects
var creationStmts = []string{
	"CREATE TABLE IF NOT EXISTS `teams` (" +
		"`team_id`							INT NOT NULL PRIMARY KEY AUTO_INCREMENT," +
		"`team_name`						VARCHAR(60) NOT NULL," +
		"`timezone`                 		VARCHAR(30)," +
		"`source_time_format`   		    VARCHAR(20)," +
		"`target_time_format`   		    VARCHAR(20)," +
		"`target_distance_unit`	  			VARCHAR(20)," +
		"`target_distance_per_stroke_unit`	VARCHAR(20)," +
		"`target_speed_unit`	  			VARCHAR(20)," +
		"`target_split_unit`  				VARCHAR(20)," +
		"UNIQUE INDEX `ind_team_name` (`team_name` ASC) VISIBLE)",
	"CREATE TABLE IF NOT EXISTS `trainings` (" +
		"`training_id`				INT NOT NULL PRIMARY KEY AUTO_INCREMENT," +
		"`team_id`					INT NOT NULL," +
		"`start_time`				DATETIME NOT NULL," +
		"`end_time`					DATETIME NOT NULL," +
		"`boat`						VARCHAR(60)," +
		"`total_distance`  			FLOAT," +
		"`total_no_strokes`			INT," +
		"`avg_split_time`  			INT," +
		"`avg_speed`				FLOAT," +
		"`avg_distance_per_stroke`	FLOAT," +
		"`path`			  			TEXT," +
		"FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE NO ACTION," +
		"UNIQUE INDEX `ind_team_time` (`team_id` ASC, `start_time` ASC) VISIBLE," +
		"UNIQUE INDEX `path` (`path` ASC) VISIBLE)",
	"CREATE TABLE IF NOT EXISTS `training_details`	(" +
		"`training_id`			INT NOT NULL," +
		"`counter`              INT NOT NULL," +
		"`time`     			DATETIME NOT NULL," +
		"`duration`				INT," +
		"`cumulated_distance`  	FLOAT," +
		"`stroke_rate`  		FLOAT," +
		"`split_time`  			INT," +
		"`speed`				FLOAT," +
		"`distance_per_stroke`	FLOAT," +
		"`latitude`  			DOUBLE," +
		"`longitude`  			DOUBLE," +
		"PRIMARY KEY (`training_id`, `counter`)," +
		"FOREIGN KEY (`training_id`) REFERENCES `trainings` (`training_id`) ON DELETE CASCADE ON UPDATE NO ACTION)",
	"CREATE VIEW IF NOT EXISTS `v_trainings` AS " +
		"SELECT " +
		"`teams`.`team_name` AS `team_name`," +
		"`teams`.`timezone` AS `timezone`," +
		"`teams`.`target_time_format` AS `target_time_format`," +
		"`trainings`.`training_id` AS `training_id`," +
		"`trainings`.`start_time` AS `start_time`," +
		"`trainings`.`end_time` AS `end_time`," +
		"`trainings`.`boat` AS `boat`," +
		"`trainings`.`total_distance` AS `total_distance`," +
		"`trainings`.`total_no_strokes` AS `total_no_strokes`," +
		"`trainings`.`avg_split_time` AS `avg_split_time`," +
		"`trainings`.`avg_speed` AS `avg_speed`," +
		"`trainings`.`avg_distance_per_stroke` AS `avg_distance_per_stroke`" +
		"FROM " +
		"(`trainings` " +
		"JOIN `teams` ON (`teams`.`team_id` = `trainings`.`team_id`))",
	"CREATE VIEW IF NOT EXISTS `v_training_details` AS " +
		"SELECT " +
		"`teams`.`team_name` AS `team_name`," +
		"`training_details`.`training_id` AS `training_id`," +
		"`training_details`.`counter` AS `counter`," +
		"`training_details`.`time` AS `time`," +
		"`training_details`.`duration` AS `duration`," +
		"`training_details`.`cumulated_distance` AS `cumulated_distance`," +
		"`training_details`.`stroke_rate` AS `stroke_rate`," +
		"`training_details`.`split_time` AS `split_time`," +
		"`training_details`.`speed` AS `speed`," +
		"`training_details`.`distance_per_stroke` AS `distance_per_stroke`," +
		"`training_details`.`latitude` AS `latitude`," +
		"`training_details`.`longitude` AS `longitude` " +
		"FROM " +
		"((`training_details` " +
		"JOIN `trainings` ON (`trainings`.`training_id` = `training_details`.`training_id`)) " +
		"JOIN `teams` ON (`teams`.`team_id` = `trainings`.`team_id`))",
}

// DB statements for the deletion of all myrowing objects
var deletionStmts = []string{
	"DROP TABLE IF EXISTS `training_details`",
	"DROP TABLE IF EXISTS `trainings`",
	"DROP TABLE IF EXISTS `teams`",
	"DROP VIEW IF EXISTS `v_trainings`",
	"DROP VIEW IF EXISTS `v_training_detailss`",
}
