package db

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	l "github.com/sirupsen/logrus"
	"gitlab.com/myrowing/datamgmt/model"
)

var log *l.Entry = l.WithFields(l.Fields{"srv": "db"})

var db *sql.DB

const (
	dbHostVar = "MYROWING_DB_HOST"
	dbNameVar = "MYROWING_DATABASE"
	dbUserVar = "MYROWING_DB_USER"
	dbPwdVar  = "MYROWING_DB_PASSWORD"
)

const (
	dbAccessTrials = 10 // number of trials to connect to the DB
	dbAccessSleep  = 3  // waiting time between trials in seconds
)

// Create creates the (empty) myrowing tables and other objects in the myrowing
// DB
func Create() (err error) {
	log.Trace("creating DB objects ...")

	if err = Open(); err != nil {
		return
	}
	defer Close()

	for i, stmt := range creationStmts {
		if _, err = db.Exec(stmt); err != nil {
			err = errors.Wrapf(err, "error executing statement no %d", i)
			log.Error(err)
			return
		}
	}

	log.Trace("DB objects created")
	return
}

// Delete deletes myrowing tables and other objects from myrowing DB
func Delete() (err error) {
	log.Trace("deleting DB objects ...")

	if err = Open(); err != nil {
		return
	}
	defer Close()

	for i, stmt := range deletionStmts {
		if _, err = db.Exec(stmt); err != nil {
			err = errors.Wrapf(err, "error executing statement no %d", i)
			log.Error(err)
			return
		}
	}

	log.Trace("DB objects deleted")
	return
}

// Open opens a connection to the myrowing DB
func Open() (err error) {
	if db, err = sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", os.Getenv(dbUserVar), os.Getenv(dbPwdVar), os.Getenv(dbHostVar), os.Getenv(dbNameVar))); err != nil {
		err = errors.Wrap(err, "cannot open DB")
		log.Error(err)
		return
	}

	// check if connection really exists since Open() seems to only check its
	// arguments. Try it several times
	for i := 0; i < dbAccessTrials; i++ {
		if err = db.Ping(); err == nil {
			log.Tracef("connected to DB after %d trials", i)
			break
		}
		time.Sleep(dbAccessSleep * time.Second)
	}
	if err != nil {
		err = errors.Wrapf(err, "could not connect to DB though it was tried %d times", dbAccessTrials)
		log.Error(err)
	}

	return
}

// Close closes the connection to the myrowing DB
func Close() {
	db.Close()
}

// AddTeam adds a team to the myrowing DB
func AddTeam(tm *model.Team) (err error) {
	res, err := db.Exec(`INSERT INTO teams
	                                (team_name,timezone,source_time_format,target_time_format,target_distance_unit,target_distance_per_stroke_unit,target_speed_unit,target_split_unit)
                                VALUES (?,?,?,?,?,?,?,?)`,
		tm.Name, tm.TimeZone, tm.SrcTimeFormat, tm.TrgTimeFormat, tm.TrgUnitDist, tm.TrgUnitDistPerStroke, tm.TrgUnitSpeed, tm.TrgUnitSplit)
	if err != nil {
		err = errors.Wrap(err, "error inserting new team in DB")
		log.Error(err)
		return
	}

	tm.ID, err = res.LastInsertId()
	if err != nil {
		err = errors.Wrap(err, "cannot retrieve last inserted ID")
		log.Error(err)
		return
	}

	return
}

// AddTraining adds a training incl. training details to the myrowing DB
func AddTraining(tr *model.Training) (err error) {
	var tx *sql.Tx
	if tx, err = db.Begin(); err != nil {
		err = errors.Wrapf(err, "cannot start DB transaction to add training for team '%s', time %s", tr.Tm.Name, tr.Start)
		log.Error(err)
		return
	}

	format := "2006-01-02 15:04:00"
	res, err := tx.Exec(`INSERT INTO trainings
  	                                (team_id,start_time,end_time,boat,total_distance,total_no_strokes,avg_split_time,avg_speed,avg_distance_per_stroke,path)
                                    VALUES (?,?,?,?,?,?,?,?,?,?)`,
		tr.Tm.ID, tr.Start.Format(format), tr.End.Format(format), tr.Boat, tr.TotalDist, tr.TotalStrokes, int(tr.AvgSplit.Seconds()), tr.AvgSpeed, tr.AvgDistPerStroke, tr.Path)
	if err != nil {
		err = errors.Wrap(err, "error inserting new training in DB")
		log.Error(err)
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			rollbackErr = errors.Wrap(err, "unable to rollback")
			log.Fatal(rollbackErr)
		}
		return
	}

	tr.ID, err = res.LastInsertId()
	if err != nil {
		err = errors.Wrap(err, "cannot retrieve last inserted ID")
		log.Error(err)
		if rollbackErr := tx.Rollback(); rollbackErr != nil {
			rollbackErr = errors.Wrap(err, "unable to rollback")
			log.Fatal(rollbackErr)
		}
		return
	}

	for i := 0; i < len(tr.Details); i++ {
		td := tr.Details[i]
		_, err = tx.Exec(`INSERT INTO training_details
									   (training_id,counter,time,duration,cumulated_distance,stroke_rate,split_time,speed, distance_per_stroke,latitude,longitude)
	                              VALUES (?,?,?,?,?,?,?,?,?,?,?)`,
			tr.ID, i+1, td.T, td.D.Milliseconds(), td.CumDist, td.StrokeRate, int(td.Split.Seconds()), td.Speed, td.DistPerStroke, td.Latitute, td.Longitude)
		if err != nil {
			err = errors.Wrapf(err, "error inserting new training details in DB for team '%s', time %s", tr.Tm.Name, tr.Start)
			log.Error(err)
			if rollbackErr := tx.Rollback(); rollbackErr != nil {
				rollbackErr = errors.Wrap(err, "unable to rollback")
				log.Fatal(rollbackErr)
			}
			return
		}
	}

	if err = tx.Commit(); err != nil {
		err = errors.Wrapf(err, "unable to commit adding training to DB for team '%s', time %s", tr.Tm.Name, tr.Start)
		log.Fatal(err)
	}

	return
}

// DelTeam removes a team from the myrowing DB. Due to the foreign key
// definitions (ON DELETE CASCASE), also all training data is removed
func DelTeam(name string) (err error) {
	_, err = db.Exec(`DELETE FROM teams WHERE team_name = ?`, name)
	if err != nil {
		err = errors.Wrapf(err, "cannot delete team '%s' from DB", name)
		log.Error(err)
		return
	}
	return
}

// DelTraining removes a training from the myrowing DB. Due to the foreign key
// definitions (ON DELETE CASCASE), also all training details are removed
func DelTraining(path string) (err error) {
	_, err = db.Exec(`DELETE FROM trainings WHERE path = ?`, path)
	if err != nil {
		err = errors.Wrapf(err, "cannot delete training '%s' from DB", path)
		log.Error(err)
		return
	}
	return
}

// GetTeam retrieves team information from the myrowing DB
func GetTeam(name string) (tm *model.Team, err error) {
	tm = new(model.Team)

	row := db.QueryRow(`SELECT team_id,team_name,timezone,source_time_format,target_time_format,target_distance_unit,target_distance_per_stroke_unit,target_speed_unit,target_split_unit FROM teams WHERE team_name = ?`, name)
	if err := row.Scan(&(tm.ID), &(tm.Name), &(tm.TimeZone), &(tm.SrcTimeFormat), &(tm.TrgTimeFormat), &(tm.TrgUnitDist), &(tm.TrgUnitDistPerStroke), &(tm.TrgUnitSpeed), &(tm.TrgUnitSplit)); err != nil {
		err = errors.Wrapf(err, "cannot read team '%s'", name)
		log.Error(err)
	}

	return
}
