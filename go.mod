module gitlab.com/myrowing/datamgmt

go 1.17

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/rjeczalik/notify v0.9.2
	github.com/sirupsen/logrus v1.8.1
	gitlab.com/mipimipi/go-utils v0.0.0-20210115163511-d99c659a024d
)

require golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
