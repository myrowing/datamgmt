# use bash
SHELL=/bin/bash

PRG=datamgmt

# set project VERSION if VERSION hasn't been passed from command line
VERSION ?= `cat ./VERSION`

# build all executables
all:
	go build .

race:
	go build -race .

.PHONY: all clean docker install lint race

lint:
	reuse lint
	golangci-lint run
	
# move all executables to /usr/bin 
install:
	install -Dm755 ./$(PRG) $(DESTDIR)/usr/bin/$(PRG)

# remove build results
clean:
	rm -f ./$(PRG)