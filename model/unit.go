package model

import (
	"fmt"
)

// Unit represents a unit of measure for speed, distance etc.
type Unit string

// unit constants
const (
	Meters    Unit = "m"
	Feet      Unit = "ft"
	KM        Unit = "km"
	Miles     Unit = "mi"
	Split500m Unit = "/500m"
	Split1mi  Unit = "/1mi"
	KMPerH    Unit = "km/h"
	MiPerH    Unit = "MPH"
	MPerS     Unit = "m/s"
)

type u2u struct {
	src, trg Unit
}

// factors for unit conversion
var convs = map[u2u]float64{
	{Meters, Feet}:        3.28,
	{Feet, Meters}:        0.31,
	{Meters, KM}:          0.001,
	{Meters, Miles}:       0.00062137,
	{KM, Meters}:          1000,
	{KM, Miles}:           0.62137,
	{Miles, Meters}:       1609.344,
	{Miles, KM}:           1.60934,
	{Split500m, Split1mi}: 3.21688,
	{Split1mi, Split500m}: 0.31086021238,
	{KMPerH, MiPerH}:      0.62137,
	{KMPerH, MPerS}:       0.27777777777778,
	{MiPerH, KMPerH}:      1.60934,
	{MiPerH, MPerS}:       0.44704,
	{MPerS, KMPerH}:       3.6,
	{MPerS, MiPerH}:       2.2369362920544,
}

// Convert converts value of unit src into unit trg
func Convert(value float64, src, trg Unit) float64 {
	if src == trg {
		return value
	}

	factor, exists := convs[u2u{src: src, trg: trg}]
	if !exists {
		return 0
	}

	return value * factor
}

// StrToUnit creates a unit from a string representation
func StrToUnit(s string) (Unit, error) {
	switch Unit(s) {
	case Feet:
		return Feet, nil
	case Meters:
		return Meters, nil
	case KM:
		return KM, nil
	case Miles:
		return Miles, nil
	case Split500m:
		return Split500m, nil
	case Split1mi:
		return Split1mi, nil
	case KMPerH:
		return KMPerH, nil
	case MiPerH:
		return MiPerH, nil
	case MPerS:
		return MPerS, nil
	default:
		err := fmt.Errorf("%s is not a valid unit", s)
		log.Fatal(err)
		return "", err
	}
}
