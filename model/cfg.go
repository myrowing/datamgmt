package model

import (
	"encoding/json"
	"io/ioutil"

	"github.com/pkg/errors"
)

type units struct {
	Distance      string `json:"distance"`
	DistPerStroke string `json:"distance_per_stroke"`
	Speed         string `json:"speed"`
	Split         string `json:"split"`
}

type config struct {
	SrcTimeFormat string `json:"source_time_format"`
	TrgTimeFormat string `json:"target_time_format"`
	TimeZone      string `json:"time_zone"`
	TrgUnits      units  `json:"target_units"`
}

// readCfg retrieves the confiuration for a team from the corresponding
// config.json file
func readCfg(path string) (cfg config, err error) {
	log.Tracef("reading config file '%s' ...", path)

	// default config
	cfg = config{
		SrcTimeFormat: "DD/MM/YY",
		TrgTimeFormat: "%d. %b %Y %H:%i",
		TimeZone:      "Europe/Berlin",
		TrgUnits: units{
			Distance:      "km",
			DistPerStroke: "m/s",
			Speed:         "m/s",
			Split:         "/500m",
		},
	}

	f, err := ioutil.ReadFile(path)
	if err != nil {
		err = errors.Wrapf(err, "config file '%s' couldn't be read", path)
		log.Fatal(err)
		return config{}, err
	}

	if err = json.Unmarshal(f, &cfg); err != nil {
		err = errors.Wrapf(err, "config file '%s' couldn't be marshalled", path)
		log.Fatal(err)
		return config{}, err
	}

	log.Tracef("read config file '%s'", path)
	return
}
