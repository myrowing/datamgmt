package model

import (
	"path/filepath"
	"time"

	l "github.com/sirupsen/logrus"
)

var log *l.Entry = l.WithFields(l.Fields{"srv": "model"})

// Team represents a rowing team
type Team struct {
	ID                   int64
	Name                 string
	TimeZone             string
	SrcTimeFormat        string
	TrgTimeFormat        string
	TrgUnitDist          Unit
	TrgUnitDistPerStroke Unit
	TrgUnitSpeed         Unit
	TrgUnitSplit         Unit
}

// New creates a new team
func New(path string) (tm *Team, err error) {
	tm = new(Team)
	tm.Name = filepath.Base(filepath.Dir(path))

	log.Tracef("creating team '%s' ...", tm.Name)

	cfg, err := readCfg(path)
	if err != nil {
		return
	}

	if tm.TrgUnitDist, err = StrToUnit(cfg.TrgUnits.Distance); err != nil {
		return
	}
	if tm.TrgUnitDistPerStroke, err = StrToUnit(cfg.TrgUnits.DistPerStroke); err != nil {
		return
	}
	if tm.TrgUnitSpeed, err = StrToUnit(cfg.TrgUnits.Speed); err != nil {
		return
	}
	if tm.TrgUnitSplit, err = StrToUnit(cfg.TrgUnits.Split); err != nil {
		return
	}

	tm.TimeZone = cfg.TimeZone
	tm.SrcTimeFormat = cfg.SrcTimeFormat
	tm.TrgTimeFormat = cfg.TrgTimeFormat

	log.Tracef("team '%s' created", tm.Name)
	return
}

// Training represents a rowing training
type Training struct {
	ID               int64
	Tm               *Team
	Start            time.Time
	End              time.Time
	Boat             string
	TotalDist        float64
	AvgSplit         time.Duration
	AvgSpeed         float64
	AvgDistPerStroke float64
	AvgStrokeRate    float64
	TotalStrokes     int
	Path             string
	Details          []TrainingDetail
}

// TrainingDetails represent one data point of a training
type TrainingDetail struct {
	T             time.Time
	D             time.Duration
	CumDist       float64
	StrokeRate    float64
	Split         time.Duration
	Speed         float64
	DistPerStroke float64
	Latitute      float64
	Longitude     float64
}
