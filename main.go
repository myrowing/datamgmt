package main

import (
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/rjeczalik/notify"
	l "github.com/sirupsen/logrus"
	"gitlab.com/mipimipi/go-utils/file"
	"gitlab.com/myrowing/datamgmt/db"
	"gitlab.com/myrowing/datamgmt/model"
	"gitlab.com/myrowing/datamgmt/parser"
)

const (
	dataDir = "/opt/myrowing"
	cfgFile = "config.json"
)

// update interval in seconds
const updInterval = 120

// extension of data files
const csvExt = ".csv"

var log *l.Entry = l.WithFields(l.Fields{"srv": "main"})

// initialLoad initially creates the DB content from all available data files
// upon start of the myrowing data managament
func initialLoad() (err error) {
	log.Trace("initially loading DB ...")

	// check is data directory exists and create it if it doesn't
	exists := false
	if exists, err = file.Exists(dataDir); err != nil {
		err = errors.Wrap(err, "cannot check existence of data directory")
		log.Fatal(err)
		return
	}
	if !exists {
		if err = os.MkdirAll(dataDir, 0666); err != nil {
			err = errors.Wrap(err, "cannot create data directory")
			log.Fatal(err)
		}
		return
	}

	var cfgPaths, dataPaths []string

	// get paths of all team config files and all training data files
	if cfgPaths, dataPaths, err = readAllPaths(); err != nil {
		return
	}

	// create DB content from these files
	if err = update(cfgPaths, dataPaths); err != nil {
		err = errors.Wrap(err, "cannot initially load trainings")
		log.Error(err)
	}

	log.Trace("DB loaded")
	return
}

// listenAndUpdate listens to changes in the data directory of myrowing. If
// relevant files have been created, changed or deleted, the DB is updated
// accordingly
func listenAndUpdate() (err error) {
	log.Trace("listening ...")

	// add watcher for inotify events for data directory. Changes will be
	// received via channel chgs
	chgs := make(chan notify.EventInfo, 1)
	if err = notify.Watch(filepath.Join(dataDir, "..."), chgs, notify.All); err != nil {
		err = errors.Wrap(err, "cannot add inotify watcher")
		log.Fatal(err)
		return
	}

	var wg sync.WaitGroup
	ticker := time.NewTicker(updInterval * time.Second)

	// semaphore to ensure that only one content update run is done at any time
	sema := make(chan struct{}, 1)

	defer func() {
		notify.Stop(chgs)
		close(chgs)
		ticker.Stop()
		close(sema)
		log.Trace("stopped listening")
	}()

	var (
		mutChanges sync.Mutex
		changes    []notify.EventInfo
	)

	for {
		select {
		case chg := <-chgs:
			// receive inotify events
			mutChanges.Lock()
			changes = append(changes, chg)
			mutChanges.Unlock()

		case <-ticker.C:
			// periodic update trigger
			wg.Add(1)
			go func() {
				sema <- struct{}{}
				defer func() {
					<-sema
					wg.Done()
				}()

				// check if there are changes at all. If yes copy changes to local table
				// protected by a mutex to avoid inconsistencies
				exists := false
				var paths []string
				mutChanges.Lock()
				if len(changes) > 0 {
					for _, chg := range changes {
						paths = append(paths, chg.Path())
					}
					changes = nil
					exists = true
				}
				mutChanges.Unlock()

				// process changes (i.e. update DB accordingly)
				if exists {
					var cfgPaths, dataPaths []string

					// extract relevant file paths from all changed, created or
					// removed paths ...
					if cfgPaths, dataPaths, err = getPathsForUpdate(paths); err != nil {
						return
					}
					// ... and update the DB accordingly
					if err = update(cfgPaths, dataPaths); err != nil {
						return
					}

				}
			}()
		}
	}
}

// getAllDataPath returns the paths of all data paths that are in the same
// directory as the config file cfgPath (i.e. it returns all training data
// files of the team that cfgPath corresponds to). This is necessary since if
// the config file was changed, all DB content for that team must be recreated
func getAllDataPaths(cfgPath string) (dataPaths []string, err error) {
	log.Tracef("retrieve all data files for '%s' ...", cfgPath)

	// check if cfgPath is really the path of a config file. Otherwise: Noting
	// to do
	if filepath.Base(cfgPath) != cfgFile {
		return
	}
	if filepath.Dir(filepath.Dir(cfgPath)) != dataDir {
		return
	}

	var paths []fs.FileInfo
	dir := filepath.Dir(cfgPath)
	if paths, err = ioutil.ReadDir(dir); err != nil {
		err = errors.Wrapf(err, "cannot read team directory '%s'", dir)
		log.Fatal(err)
		return
	}
	for _, fi := range paths {
		if fi.IsDir() || filepath.Ext(fi.Name()) != csvExt {
			continue
		}
		dataPaths = append(dataPaths, filepath.Join(dir, fi.Name()))
	}

	log.Tracef("retrieved all data files for '%s'", cfgPath)
	return
}

// getPathsForUpdate takes a list of all changed, created and removed paths and
// extracts the relevant config and data files from that
func getPathsForUpdate(paths []string) (cfgPaths, dataPaths []string, err error) {
	log.Trace("determining relevant paths for DB update ...")

	for _, path := range paths {
		// path is only relevant if it represents a file
		if fi, err := os.Stat(path); err == nil && fi.IsDir() {
			continue
		}

		// path is only relevant if it's located in a direct sub directory of the data directory
		if filepath.Dir(filepath.Dir(path)) != dataDir {
			continue
		}

		// check if path is a config file
		if filepath.Base(path) == cfgFile {
			cfgPaths = append(cfgPaths, path)

			// if config file was changed, all relevant trainings must be
			// updated. Thus: Add all data files
			var paths []string
			if paths, err = getAllDataPaths(path); err != nil {
				return
			}
			dataPaths = append(dataPaths, paths...)
			continue
		}

		// check if path is a data file
		if filepath.Ext(path) == csvExt {
			log.Tracef("found data file '%s'", path)

			// check if config file exists in same directory
			exists := false
			if exists, err = file.Exists(filepath.Join(filepath.Dir(path), cfgFile)); err != nil {
				err = errors.Wrapf(err, "cannot check existence of config file in '%s'", filepath.Dir(path))
				log.Fatal(err)
				return
			}
			if !exists {
				log.Trace("config file does not exist: ignore data file")
				continue
			}

			dataPaths = append(dataPaths, path)
		}
	}

	// remove duplicates
	sort.Strings(cfgPaths)
	removeDuplicates(&cfgPaths)
	sort.Strings(dataPaths)
	removeDuplicates(&dataPaths)

	log.Trace("determined relevant paths for DB update")
	return
}

// readAllPaths determines all relevant config file and data file paths for the
// initial load to the myrowing DB
func readAllPaths() (cfgPaths, dataPaths []string, err error) {
	log.Trace("determining all relevant paths for initial DB load ...")

	var tmDirs []fs.FileInfo
	if tmDirs, err = ioutil.ReadDir(dataDir); err != nil {
		err = errors.Wrap(err, "cannot read data directory")
		log.Fatal(err)
		return
	}

	for _, tmDir := range tmDirs {
		if !tmDir.IsDir() {
			continue
		}

		path := filepath.Join(dataDir, tmDir.Name())

		exists := false
		cfgPath := filepath.Join(path, cfgFile)
		if exists, err = file.Exists(cfgPath); err != nil {
			err = errors.Wrapf(err, "cannot check existence of config file in '%s'", path)
			log.Fatal(err)
			return
		}
		if !exists {
			log.Infof("config file '%s' does not exist: ignoring directory", cfgPath)
			continue
		}
		cfgPaths = append(cfgPaths, cfgPath)

		var trFiles []fs.FileInfo
		if trFiles, err = ioutil.ReadDir(path); err != nil {
			err = errors.Wrapf(err, "cannot read team directory '%s'", path)
			log.Fatal(err)
			return
		}

		for _, trFile := range trFiles {
			if filepath.Ext(trFile.Name()) != csvExt {
				continue
			}
			dataPaths = append(dataPaths, filepath.Join(path, trFile.Name()))
		}
	}

	log.Trace("determined all relevant paths for initial DB load")
	return
}

// removeDuplicates removes duplicates from paths
func removeDuplicates(paths *[]string) {
	n := len(*paths)
	if n <= 1 {
		return
	}
	j := 1
	for i := 1; i < n; i++ {
		if (*paths)[i] != (*paths)[i-1] {
			(*paths)[j] = (*paths)[i]
			j++
		}
	}

	(*paths) = (*paths)[0:j]
}

// updates takes changed, created or removed config or data file paths and
// updates the myrowing DB accordingly
func update(cfgPaths, dataPaths []string) (err error) {
	log.Trace("updating DB ...")

	if len(cfgPaths) == 0 && len(dataPaths) == 0 {
		return
	}

	if err = db.Open(); err != nil {
		return
	}
	defer db.Close()

	for _, path := range cfgPaths {
		if err = db.DelTeam(filepath.Base(filepath.Dir(path))); err != nil {
			return
		}
		exists := false
		if exists, err = file.Exists(path); err != nil {
			err = errors.Wrapf(err, "cannot check existence of config file in '%s'", path)
			log.Fatal(err)
			return
		}
		if exists {
			tm, err := model.New(path)
			if err != nil {
				return err
			}
			if err = db.AddTeam(tm); err != nil {
				return err
			}
		}
	}

	for _, path := range dataPaths {
		if err = db.DelTraining(path); err != nil {
			return
		}
		exists := false
		if exists, err = file.Exists(path); err != nil {
			err = errors.Wrapf(err, "cannot check existence of training file in '%s'", path)
			log.Fatal(err)
			return
		}
		if exists {
			var tm *model.Team
			if tm, err = db.GetTeam(filepath.Base(filepath.Dir(path))); err != nil {
				return err
			}
			var p parser.Parser
			if p, err = parser.New(path); err != nil {
				return err
			}
			var tr *model.Training
			if tr, err = p.Read(tm, filepath.Join(path)); err != nil {
				return err
			}
			if err = db.AddTraining(tr); err != nil {
				return err
			}
		}
	}

	log.Trace("DB updated")
	return
}

func main() {
	if err := setupLogging(); err != nil {
		os.Exit(1)
	}

	// delete potentially existing old DB objects
	if err := db.Delete(); err != nil {
		os.Exit(1)
	}

	// create DB objects
	if err := db.Create(); err != nil {
		// clean up DB and exit
		_ = db.Delete()
		os.Exit(1)
	}

	// load initial data into DB
	if err := initialLoad(); err != nil {
		// clean up DB and exit
		_ = db.Delete()
		os.Exit(1)
	}

	// start listen and update loop
	if err := listenAndUpdate(); err != nil {
		// clean up DB and exit
		_ = db.Delete()
		os.Exit(1)
	}
}
