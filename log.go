package main

import (
	"fmt"
	"os"

	l "github.com/sirupsen/logrus"
)

const logFilepath = "/var/log/myrowing/datamgmt.log"

const logLevelVar = "MYROWING_LOG_LEVEL"

// setupLogging sets up logging into file logFilepath. If the log file does not
// exist yet, it is created.
func setupLogging() (err error) {
	// set log level string
	logLevel := os.Getenv(logLevelVar)
	if len(logLevel) == 0 {
		logLevel = "error"
	}

	// set up logging: no log entries possible before this statement!
	level, err := l.ParseLevel(logLevel)
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}

	// create or open file for write & append
	f, err := os.OpenFile(logFilepath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Printf("%v\n", err)
		return
	}

	l.SetOutput(f)
	l.SetLevel(level)
	return
}
