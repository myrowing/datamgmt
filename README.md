# Data Management (datamgmt)

`datamgmt` keeps a [MariaDB](https://mariadb.org/) database up to data with training data stored in [CSV files](https://en.wikipedia.org/wiki/Comma-separated_values). This means, that the DB data is added if new CSV files are copied into the corresponding folders or deleted if files are removed from the folders.

It is recommended to run `datamgmt` as Docker container (get the [latest image](https://gitlab.com/myrowing/datamgmt/container_registry)).

## Environment Variables

| Variable | Description |
|----------|-------------|
| `MYROWING_DB_USER` | Name of the DB user |
| `MYROWING_DB_PASSWORD` | Password of the DB user |
| `MYROWING_DB_HOST` | Host address of the DB server |
| `MYROWING_LOG_LEVEL` | Log level for `datamgmt`. Possible values are: `trace`, `debug`, `ìnfo`, `warn`, `error`, `fatal`, `panic` |

## Files and Directories

| File / Directory | Description |
|------------------|-------------|
| `/var/myrowing/log/datamgmt.log` | Log file |
| `/opt/myrowing` | Contains the team folders as direct sub directories. Each team folder contains the CSV files for that team |
| `/opt/myrowing/<TEAM-DIRECTORY>/config.json` | Configuration file for that team in [JSON format](https://en.wikipedia.org/wiki/JSON) |

### Configuration File

| Attribute | Description |
|-----------|-------------|
| `source_time_format` | Date format of the CSV file. Possible values: `MM/DD/YY`, `DD/MM/YY` |
| `target_time_format` | Date/time format to be used in dashboards in [MariaDB/MySQL syntax](https://www.mysqltutorial.org/mysql-date_format/) |
| `time_zone` | Time zone of the training data |
| `units.distance` | Unit for distances. Possible values: `km`, `m`, `mi` |
| `units.distance_per_stroke` | Unit for distances per stroke. Possible values: `m`, `ft` |
| `units.speed` | Unit for speed. Possible values: `km/h`, `m/s`, `MPH` |
| `units.split` | Unit for split. Possible values: `/500m`, `1mi` |

Example:

    {
        "source_time_format": "DD/MM/YY",
        "target_time_format": "%d. %b %Y %H:%i",
        "time_zone": "Europe/Berlin",
        "target_units": {
            "distance": "km",
            "distance_per_stroke": "m",
            "speed": "km/h",
            "split": "/500m"
        }  
    }

## Limitations

Currently, only CSV files from the [Nielsen-Kellerman SpeedCoach](https://nksports.com/category-strokecoach-and-speedcoach) are supported.

